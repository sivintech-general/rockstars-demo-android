package com.sivintech.rockstars.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sivintech.rockstars.MainActivity;
import com.sivintech.rockstars.R;

import java.io.File;

/**
 * A profile fragment provide to change user photo and full name.
 */
public class ProfileFragment extends Fragment {
    public static final String TAG = "ProfileFragment";
    public static final int PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL = 101;
    public static final int REQUEST_SELECT_PICTURE = 102;

    private ImageView mAvatar;
    private EditText mFullName;
    private Uri mPhotoUri;

    public ProfileFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (! dir.exists()){
            if (! dir.mkdirs()){
                Log.d(TAG, "failed to create directory");
            }
        }
        mPhotoUri = Uri.parse("file://" + dir.getPath() + File.separator + "user_photo.jpg");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mAvatar = (ImageView) rootView.findViewById(R.id.photo);
        mAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(getContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showExplanation();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL);
                    }
                } else {
                    performSelectPicture();
                }
            }
        });

        mFullName = (EditText) rootView.findViewById(R.id.full_name);
        SharedPreferences preferences = getActivity().getSharedPreferences(MainActivity.PREF_FILE_NAME, Context.MODE_PRIVATE);
        mFullName.setText(preferences.getString(MainActivity.USER_NAME, ""));


        mAvatar.setImageURI(mPhotoUri);

        return rootView;
    }

    private void performSelectPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
        intent.putExtra("return-data", true);

        getActivity().startActivityForResult(intent, REQUEST_SELECT_PICTURE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_done) {
            saveInfo();
            return true;
        }
        Log.i("profile", "onOptionsItemSelected");

        return super.onOptionsItemSelected(item);
    }

    void showExplanation() {
        Snackbar snackbar = Snackbar
                .make(mAvatar, R.string.storage_access_explanation, Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });

        snackbar.show();
    }

    private void saveInfo() {
        SharedPreferences settings = getActivity().getSharedPreferences(MainActivity.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(MainActivity.USER_NAME, mFullName.getText().toString());
        editor.putString(MainActivity.USER_PHOTO_URI, mPhotoUri.toString());
        editor.commit();

        Toast.makeText(getContext(), R.string.profile_changed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                performSelectPicture();
            } else {
                showExplanation();
            }
            return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProfileFragment.REQUEST_SELECT_PICTURE) {
            mAvatar.setImageURI(null);
            mAvatar.setImageURI(mPhotoUri);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
