package com.sivintech.rockstars.web;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.sivintech.rockstars.models.Rockstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class Service {
	public static final String TAG = "Service";

	public static final String SERVICE = "http://54.72.181.8/yolo";
	private static final String METHOD_GET_CONTACTS = "/contacts.json";

	private HttpHelper helper = new HttpHelper();
	private WebMethodFailedListener mListener;

	public Service(WebMethodFailedListener listener) {
		mListener = listener;
	}
	
	public ArrayList<Rockstar> getRockstars() {
		String json = helper.getDataFromUrl(mListener, SERVICE + METHOD_GET_CONTACTS, HttpHelper.RequestType.GET, null, null);
		try {
			JSONObject file = new JSONObject(json);
			JSONArray array = file.getJSONArray("contacts");
			ArrayList<Rockstar> result = new ArrayList<Rockstar>(array.length());
			for (int i = 0; i < array.length(); i++) {
				RockstarEntry entry = new RockstarEntry(array.getJSONObject(i));
				Bitmap bitmap = null;
				try {
					bitmap = BitmapFactory.decodeStream((InputStream) new URL(entry.getImageUrl()).getContent());
				} catch (Exception e) {
					e.printStackTrace();
				}
				result.add(new Rockstar(entry.getFirstName(), entry.getLastName(), entry.getStatus(), bitmap));
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<Rockstar>(0);
	}

	private class RockstarEntry extends JSONObject {
		private static final String KEY_FIRST_NAME = "firstname";
		private static final String KEY_LAST_NAME = "lastname";
		private static final String KEY_STATUS = "status";
		private static final String KEY_PHOTO = "hisface";

		public RockstarEntry(JSONObject obj) throws JSONException {
			super(obj.toString());
		}

		public String getFirstName() {
			try {
				return getString(KEY_FIRST_NAME);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		public String getLastName() {
			try {
				return getString(KEY_LAST_NAME);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		public String getStatus() {
			try {
				return getString(KEY_STATUS);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		public String getImageUrl() {
			try {
				return SERVICE + "/" + getString(KEY_PHOTO);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
