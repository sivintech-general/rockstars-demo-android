package com.sivintech.rockstars.database;

import android.provider.BaseColumns;

public class DBRockstarColumns implements BaseColumns {
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String STATUS = "status";
	public static final String THUMBNAIL = "picture";
	public static final String PICTURE_URI = "picture_uri";
}
