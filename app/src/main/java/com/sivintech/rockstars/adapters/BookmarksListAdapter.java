package com.sivintech.rockstars.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sivintech.rockstars.R;
import com.sivintech.rockstars.models.Rockstar;

import java.util.ArrayList;

public class BookmarksListAdapter extends BaseAdapter {

	public interface DeleteCallback {
		public void delete(Rockstar rockstar);
	}

	private ArrayList<Rockstar> mRockstars;
	private DeleteCallback mCallback;
	private LayoutInflater mInflater;

	public BookmarksListAdapter(ArrayList<Rockstar> rockstars, LayoutInflater inflater) {
		mRockstars = rockstars;
		mInflater = inflater;
	}

	public void setRockstars(ArrayList<Rockstar> rockstars) {
		this.mRockstars = rockstars;
	}

	public void setDeleteCallback(DeleteCallback callback) {
		this.mCallback = callback;
	}

	@Override
	public int getCount() {
		return mRockstars == null ? 0 : mRockstars.size();
	}

	@Override
	public Object getItem(int position) {
		return mRockstars.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
		private ImageView avatar;
		private TextView name;
		private TextView status;
		private ImageButton delete;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_bookmark, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
			holder.avatar = (ImageView) convertView.findViewById(R.id.imageView);
			holder.name = (TextView) convertView.findViewById(R.id.textView);
			holder.status = (TextView) convertView.findViewById(R.id.textView2);
			holder.delete = (ImageButton) convertView.findViewById(R.id.imageButton);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Rockstar rockstar = mRockstars.get(position);
		holder.avatar.setImageBitmap(rockstar.getPhoto());
		holder.name.setText(rockstar.getFullName());
		holder.status.setText(rockstar.getStatus());
		holder.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCallback != null) {
					mCallback.delete(rockstar);
				}
			}
		});

		return convertView;
	}

	public ArrayList<Rockstar> getmRockstars() {
		return mRockstars;
	}

	public void setmRockstars(ArrayList<Rockstar> mRockstars) {
		this.mRockstars = mRockstars;
	}
}
