package com.sivintech.rockstars.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.sivintech.rockstars.models.Rockstar;

import java.util.ArrayList;

public class DBRockstarsHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "rockstars_db";
    public static final int DATABASE_VERSION = 1;
    public static final String ROCKSTARS_TABLE = "rockstars_table";

    private static DBRockstarsHelper mDbHelper;
    private static SQLiteDatabase mDatabase;

    private DBRockstarsHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mDatabase = getWritableDatabase();
    }

    public static DBRockstarsHelper getInstance(Context context) {
        if (mDbHelper == null || mDatabase == null || !mDatabase.isOpen()) {
            mDbHelper = new DBRockstarsHelper(context);
        }
        return mDbHelper;
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase db = null;
        while (db == null) {
            try {
                db = super.getWritableDatabase();
            } catch (SQLiteException e) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e1) {
                }
            }
        }
        return db;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ROCKSTARS_TABLE + " (" + DBRockstarColumns._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRockstarColumns.FIRST_NAME + " TEXT, "
                + DBRockstarColumns.LAST_NAME + " TEXT, "
                + DBRockstarColumns.STATUS + " TEXT, "
                + DBRockstarColumns.THUMBNAIL + " BLOB, "
                + DBRockstarColumns.PICTURE_URI + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public ArrayList<Rockstar> getAllRockstars() {
        ArrayList<Rockstar> rockstars = null;
        Cursor cursor = mDatabase.query(ROCKSTARS_TABLE, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            rockstars = new ArrayList<Rockstar>(cursor.getCount());
            do {
                rockstars.add(provideRockstar(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return rockstars;
    }

    public Rockstar provideRockstar(Cursor cursor) {
        if (cursor != null) {
            String picture = cursor.getString(cursor.getColumnIndex(DBRockstarColumns.PICTURE_URI));
            byte[] byteArray = cursor.getBlob(cursor.getColumnIndex(DBRockstarColumns.THUMBNAIL));
            Uri pictureUri = null;
            if (picture != null) {
                pictureUri = Uri.parse(picture);
            }

            Rockstar rockstar = new Rockstar(
                    cursor.getString(cursor.getColumnIndex(DBRockstarColumns.FIRST_NAME)),
                    cursor.getString(cursor.getColumnIndex(DBRockstarColumns.LAST_NAME)),
                    cursor.getString(cursor.getColumnIndex(DBRockstarColumns.STATUS)),
                    Rockstar.getByteArrayAsBitmap(byteArray),
                    pictureUri);

            return rockstar;
        }
        return null;
    }

    public long addRockstar(Rockstar rockstar) {
        ContentValues values = new ContentValues();
        values.put(DBRockstarColumns.FIRST_NAME, rockstar.getFirstName());
        values.put(DBRockstarColumns.LAST_NAME, rockstar.getLastName());
        values.put(DBRockstarColumns.STATUS, rockstar.getStatus());
        values.put(DBRockstarColumns.THUMBNAIL, Rockstar.getBitmapAsByteArray(rockstar.getPhoto()));
        if (rockstar.getPhotoUri() != null) {
            values.put(DBRockstarColumns.PICTURE_URI, rockstar.getPhotoUri().toString());
        }
        return mDatabase.insert(ROCKSTARS_TABLE, null, values);
    }

    public int deleteRockstar(Rockstar rockstar) {
        return mDatabase.delete(ROCKSTARS_TABLE, DBRockstarColumns.FIRST_NAME + " = ? AND "
                        + DBRockstarColumns.LAST_NAME + " = ?",
                new String[]{ rockstar.getFirstName(), rockstar.getLastName() });
    }

    public int deleteRockstar(long id) {
        return mDatabase.delete(ROCKSTARS_TABLE, DBRockstarColumns._ID + " = ?",
                new String[]{String.valueOf(id)});
    }
}
