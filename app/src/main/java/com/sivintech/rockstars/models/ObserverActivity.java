package com.sivintech.rockstars.models;

public interface ObserverActivity {
    public void onTabContentChanged(int section);
}
