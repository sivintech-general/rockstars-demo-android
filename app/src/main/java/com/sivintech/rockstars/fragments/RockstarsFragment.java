package com.sivintech.rockstars.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.sivintech.rockstars.R;
import com.sivintech.rockstars.adapters.RockstarsListAdapter;
import com.sivintech.rockstars.database.DBRockstarsHelper;
import com.sivintech.rockstars.dialogs.ErrorDialog;
import com.sivintech.rockstars.models.ObserverActivity;
import com.sivintech.rockstars.models.Rockstar;
import com.sivintech.rockstars.web.Service;
import com.sivintech.rockstars.web.WebMethodFailedListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * A rockstars fragment containing a list of all rockstars received from server, and filter.
 */
public class RockstarsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        WebMethodFailedListener, TextView.OnEditorActionListener, SearchView.OnQueryTextListener,
        RockstarsListAdapter.CheckBoxCallback, ObservableTab {
    public static final int SECTION = 0;

    private Service mService;
    private ObserverActivity mActivity;
    private ArrayList<Rockstar> mRockstars;
    private DBRockstarsHelper mDBHelper;

    private SearchView mSearchView;
    private RockstarsListAdapter listAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public RockstarsFragment() {
        mService = new Service(this);
    }

    public RockstarsFragment(ArrayList<Rockstar> rockstars) {
        mRockstars = rockstars;
        mService = new Service(this);
        mDBHelper = DBRockstarsHelper.getInstance(getContext());
        ArrayList<Rockstar> savedRockstars = mDBHelper.getAllRockstars();
        Set<String> names = new HashSet<String>(0);
        if (savedRockstars != null && savedRockstars.size() > 0) {
            names = new HashSet<String>(savedRockstars.size());
            for (Rockstar rockstar : savedRockstars) {
                names.add(rockstar.getFullName());
            }
        }
        if (mRockstars != null) {
            for (Rockstar rockstar : mRockstars) {
                rockstar.setSaved(names.contains(rockstar.getFullName()));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mActivity = (ObserverActivity) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnTabContentChangedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rockstars, container, false);

        mSearchView = new SearchView(getActivity());
        mSearchView.onActionViewCollapsed();
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setQueryHint(getString(R.string.filter_hint));
        mSearchView.setOnQueryTextListener(this);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listAdapter = new RockstarsListAdapter(mRockstars, getActivity().getLayoutInflater());
        listAdapter.setCheckBoxCallback(this);
        listView.setAdapter(listAdapter);
        listView.addHeaderView(mSearchView, null, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return rootView;
    }

    @Override
    public void onRefresh() {
        UpdateRockstarsTask task = new UpdateRockstarsTask();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    @Override
    public void onWebMethodFailed(int code, String message) {
        if (message == null){
            message = getString(R.string.error_internet);
        }
        ErrorDialog dialog = new ErrorDialog();
        dialog.setErrorMessage(message);
        dialog.show(getFragmentManager(), "error");

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        listAdapter.setFilter(query);
        listAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.setFilter(newText);
        listAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean checkSaved(boolean isChecked, Rockstar rockstar) {
        if (isChecked) {
            mDBHelper.addRockstar(rockstar);
        } else {
            mDBHelper.deleteRockstar(rockstar);
        }
        update();
        mActivity.onTabContentChanged(SECTION);
        return true;
    }

    @Override
    public void update() {
        ArrayList<Rockstar> savedRockstars = mDBHelper.getAllRockstars();
        Set<String> names = new HashSet<String>(0);
        if (savedRockstars != null && savedRockstars.size() > 0) {
            names = new HashSet<String>(savedRockstars.size());
            for (Rockstar rockstar : savedRockstars) {
                names.add(rockstar.getFullName());
            }
        }
        if (mRockstars != null) {
            for (Rockstar rockstar : mRockstars) {
                rockstar.setSaved(names.contains(rockstar.getFullName()));
            }
        }
        listAdapter.setRockstars(mRockstars);
        listAdapter.notifyDataSetChanged();
    }

    private class UpdateRockstarsTask extends AsyncTask<Void, Void, ArrayList<Rockstar>> {

        public UpdateRockstarsTask() {}

        @Override
        protected ArrayList<Rockstar> doInBackground(Void... params) {
            return mService.getRockstars();
        }

        @Override
        protected void onPostExecute(ArrayList<Rockstar> result) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }

            mRockstars = result;
            update();
        }
    }
}
