package com.sivintech.rockstars.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sivintech.rockstars.R;
import com.sivintech.rockstars.fragments.BookmarksFragment;
import com.sivintech.rockstars.fragments.ProfileFragment;
import com.sivintech.rockstars.fragments.RockstarsFragment;
import com.sivintech.rockstars.models.Rockstar;

import java.util.ArrayList;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private FragmentActivity activity;
    private ArrayList<Fragment> fragments = new ArrayList<>(3);

    public SectionsPagerAdapter(FragmentActivity activity) {
        super(activity.getSupportFragmentManager());
        this.activity = activity;
        fragments.add(new RockstarsFragment());
        fragments.add(new BookmarksFragment());
        fragments.add(new ProfileFragment());
    }

    public SectionsPagerAdapter(FragmentActivity activity, ArrayList<Rockstar> rockstars) {
        super(activity.getSupportFragmentManager());
        this.activity = activity;
        fragments.add(new RockstarsFragment(rockstars));
        fragments.add(new BookmarksFragment());
        fragments.add(new ProfileFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return activity.getString(R.string.rockstars);
            case 1:
                return activity.getString(R.string.bookmarks);
            case 2:
                return activity.getString(R.string.profile);
        }
        return null;
    }
}
