package com.sivintech.rockstars.fragments;

public interface ObservableTab {
    public void update();
}
