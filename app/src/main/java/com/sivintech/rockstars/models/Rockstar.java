package com.sivintech.rockstars.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.ByteArrayOutputStream;

public class Rockstar {
    private String firstName;
    private String lastName;
    private String status;
    private Bitmap photo;
    private Uri photoUri;
    private boolean saved;

    public Rockstar(String firstName, String lastName, String status, Bitmap photo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.photo = photo;
    }

    public Rockstar(String firstName, String lastName, String status, Bitmap photo, Uri photoUri) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.photo = photo;
        this.photoUri = photoUri;
        this.saved = true;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        String fullName = firstName;
        if (fullName != null && fullName.length() > 0) {
            fullName += " " + lastName;
        } else {
            fullName = lastName;
        }
        return fullName;
    }

    public String getStatus() {
        return status;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public static Bitmap getByteArrayAsBitmap(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }
}
