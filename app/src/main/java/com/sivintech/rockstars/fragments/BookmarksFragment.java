package com.sivintech.rockstars.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sivintech.rockstars.R;
import com.sivintech.rockstars.adapters.BookmarksListAdapter;
import com.sivintech.rockstars.database.DBRockstarsHelper;
import com.sivintech.rockstars.models.ObserverActivity;
import com.sivintech.rockstars.models.Rockstar;

/**
 * A bookmarks fragment containing a list of saved rockstars.
 */
public class BookmarksFragment extends Fragment implements BookmarksListAdapter.DeleteCallback, ObservableTab {
    public static final int SECTION = 1;

    private ObserverActivity mActivity;
    private DBRockstarsHelper mDBHelper;
    private BookmarksListAdapter listAdapter;

    public BookmarksFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mDBHelper = DBRockstarsHelper.getInstance(getContext());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mActivity = (ObserverActivity) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnTabContentChangedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bookmarks, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listAdapter = new BookmarksListAdapter(mDBHelper.getAllRockstars(), getActivity().getLayoutInflater());
        listAdapter.setDeleteCallback(this);
        listView.setAdapter(listAdapter);

        return rootView;
    }

    @Override
    public void delete(Rockstar rockstar) {
        mDBHelper.deleteRockstar(rockstar);
        listAdapter.getmRockstars().remove(rockstar);
        listAdapter.notifyDataSetChanged();
        mActivity.onTabContentChanged(SECTION);
    }

    @Override
    public void update() {
        listAdapter.setRockstars(mDBHelper.getAllRockstars());
        listAdapter.notifyDataSetChanged();
    }
}
