package com.sivintech.rockstars.web;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpHelper {
	enum RequestType {
		GET, POST, PUT, DELETE
	}
	
	public HttpHelper() {}

	public HttpResponseWrapper getResponse(String url, RequestType type,
			Hashtable<String, Object> params, List<NameValuePair> headers) {
		HttpResponseWrapper result = new HttpResponseWrapper();
		HttpResponse response = null;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 15000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			int timeoutSocket = 20000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			DefaultHttpClient client = new DefaultHttpClient(httpParameters);
			client = sslClient(client);
			HttpRequestBase request = getRequestMethod(type, url);
			if (headers != null) {
				for (int i = 0; i < headers.size(); i++) {
					request.addHeader(headers.get(i).getName(), headers.get(i).getValue());
				}
			}

			addDefaultHeaders(request);

			if (params != null) {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				for (String key : params.keySet()) {
					nameValuePairs.add(new BasicNameValuePair(key, params.get(key).toString()));
				}

				((HttpEntityEnclosingRequestBase) request)
						.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}
			response = client.execute(request);

			result.status = Statuses.Ok;
			result.response = response;
		} catch (SocketTimeoutException e) {
			result.status = Statuses.Timeout;
		} catch (ConnectTimeoutException e) {
			result.status = Statuses.Timeout;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private void addDefaultHeaders(HttpRequestBase request) {
		request.addHeader("Accept-Charset", "utf-8");
	}

	private HttpRequestBase getRequestMethod(RequestType type, String url) {
		switch (type) {
		case GET:
			return new HttpGet(url);
		case POST:
			return new HttpPost(url);
		case PUT:
			return new HttpPut(url);
		case DELETE:
			return new HttpDelete(url);
		default:
			return null;
		}
	}

	private DefaultHttpClient sslClient(HttpClient client) {
		try {
			X509TrustManager tm = new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {

				}
			};
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[] { tm }, null);
			SSLSocketFactory ssf = new MySSLSocketFactory(ctx);
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager ccm = client.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 443));
			return new DefaultHttpClient(ccm, client.getParams());
		} catch (Exception ex) {
			return null;
		}
	}

	public class MySSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore)
				throws NoSuchAlgorithmException, KeyManagementException,
				KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {
					return (X509Certificate[]) null;
				}

				@Override
				public void checkClientTrusted(
						X509Certificate[] arg0, String arg1)
						throws java.security.cert.CertificateException {

				}

				@Override
				public void checkServerTrusted(
						X509Certificate[] chain,
						String authType)
						throws java.security.cert.CertificateException {

				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		public MySSLSocketFactory(SSLContext context)
				throws KeyManagementException, NoSuchAlgorithmException,
				KeyStoreException, UnrecoverableKeyException {
			super(null);
			sslContext = context;
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port,
				boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host,
					port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}

	private static class HttpResponseWrapper {
		private HttpResponseWrapper() {
			status = Statuses.Error;
		}

		HttpResponse response;
		Statuses status;

	}

	private static enum Statuses {
		Ok, Error, Timeout
	}

	public String getDataFromUrl(WebMethodFailedListener listener, String url,
			RequestType type, Hashtable<String, Object> params,List<NameValuePair> headers) {
		try {
			HttpResponseWrapper result = getResponse(url, type, params, headers);
			while (result.status == Statuses.Timeout) {
				result = getResponse(url, type, params, null);
			}
			if (result.status == Statuses.Ok) {
				HttpResponse response = result.response;
				int code = response.getStatusLine().getStatusCode();
				if (code < 300) {
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					response.getEntity().writeTo(os);
					String data = os.toString();
					return data;
				} else {
					if (code < 500) {
						ByteArrayOutputStream os = new ByteArrayOutputStream();
						response.getEntity().writeTo(os);
						String json = os.toString();
						if(json != null && json.length() > 0){
							try {
								JSONObject object = new JSONObject(json);
								JSONArray array = object.optJSONArray("error");
								String message = "";
								for (int i = 0; i < array.length(); i++) {
									message += array.getString(i) + "\n";
								}						
								listener.onWebMethodFailed(code, message);								
							} catch (Exception e) {
								e.printStackTrace();
								JSONObject object = new JSONObject(json);
								String message = object.getString("error");
								listener.onWebMethodFailed(code, message);
							}
						} else {
							listener.onWebMethodFailed(code, os.toString());
						}
					} else {
						listener.onWebMethodFailed(code, null);
					}
				}
			} else {
				listener.onWebMethodFailed(WebMethodFailedListener.UNKNOWN_ERROR, null);
			}
		} catch (Exception e) {
			listener.onWebMethodFailed(WebMethodFailedListener.UNKNOWN_ERROR, null);
			e.printStackTrace();
		}
		return null;
	}
}
