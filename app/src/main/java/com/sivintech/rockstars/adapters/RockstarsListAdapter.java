package com.sivintech.rockstars.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sivintech.rockstars.R;
import com.sivintech.rockstars.models.Rockstar;

import java.util.ArrayList;

public class RockstarsListAdapter extends BaseAdapter {

	public interface CheckBoxCallback {
		public boolean checkSaved(boolean isChecked, Rockstar rockstar);
	}

	private ArrayList<Rockstar> mRockstarsOriginal;
	private ArrayList<Rockstar> mRockstars;
	private String mFilter = "";
	private CheckBoxCallback mCallback;
	private LayoutInflater mInflater;

	public RockstarsListAdapter(ArrayList<Rockstar> rockstars, LayoutInflater inflater) {
		mRockstarsOriginal = rockstars;
		if (mRockstarsOriginal != null) {
			mRockstars = new ArrayList<Rockstar>(mRockstarsOriginal);
		} else {
			mRockstars = new ArrayList<Rockstar>(0);
		}
		mInflater = inflater;
	}

	public void setRockstars(ArrayList<Rockstar> rockstars) {
		this.mRockstarsOriginal = rockstars;
	}

	public ArrayList<Rockstar> getRockstars() {
		return mRockstars;
	}

	public void setCheckBoxCallback(CheckBoxCallback callback) {
		this.mCallback = callback;
	}

	public void setFilter(String query) {
		this.mFilter = query;
	}

	@Override
	public int getCount() {
		return mRockstars == null ? 0 : mRockstars.size();
	}

	@Override
	public Object getItem(int position) {
		return mRockstars.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
		private ImageView avatar;
		private TextView name;
		private TextView status;
		private CheckBox saved;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_rockstar, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
			holder.avatar = (ImageView) convertView.findViewById(R.id.imageView);
			holder.name = (TextView) convertView.findViewById(R.id.textView);
			holder.status = (TextView) convertView.findViewById(R.id.textView2);
			holder.saved = (CheckBox) convertView.findViewById(R.id.checkBox);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Rockstar rockstar = mRockstars.get(position);
		holder.avatar.setImageBitmap(rockstar.getPhoto());
		holder.name.setText(rockstar.getFullName());
		holder.status.setText(rockstar.getStatus());
		holder.saved.setOnCheckedChangeListener(null);
		holder.saved.setChecked(rockstar.isSaved());
		holder.saved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (mCallback != null) {
					mCallback.checkSaved(isChecked, rockstar);
				}
			}
		});

		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		if (mFilter == null || mFilter.length() == 0) {
			if (mRockstarsOriginal != null) {
				mRockstars = new ArrayList<Rockstar>(mRockstarsOriginal);
			} else {
				mRockstars = new ArrayList<Rockstar>(0);
			}
		} else {
			mRockstars = new ArrayList<Rockstar>();
			if (mRockstarsOriginal != null) {
				for (Rockstar rockstar : mRockstarsOriginal) {
					if (rockstar.getFullName().toLowerCase().contains(mFilter.toLowerCase())) {
						mRockstars.add(rockstar);
					}
				}
			}
		}

		super.notifyDataSetChanged();
	}
}
