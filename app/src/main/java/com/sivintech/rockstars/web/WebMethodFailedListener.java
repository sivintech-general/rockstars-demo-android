package com.sivintech.rockstars.web;

/**
 * Interface used to process failed web requests
 */
public interface WebMethodFailedListener {
	public static final int UNKNOWN_ERROR = -1;
	public static final int OK = 200;
	
	/**
	 * Fired when web request is failed
	 * @param code status code
	 * @param message error message, may be null
	 */
	public void onWebMethodFailed(int code, String message);
}
