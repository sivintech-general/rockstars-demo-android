package com.sivintech.rockstars;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.sivintech.rockstars.adapters.SectionsPagerAdapter;
import com.sivintech.rockstars.dialogs.ErrorDialog;
import com.sivintech.rockstars.fragments.BookmarksFragment;
import com.sivintech.rockstars.fragments.ObservableTab;
import com.sivintech.rockstars.fragments.ProfileFragment;
import com.sivintech.rockstars.fragments.RockstarsFragment;
import com.sivintech.rockstars.models.ObserverActivity;
import com.sivintech.rockstars.models.Rockstar;
import com.sivintech.rockstars.web.Service;
import com.sivintech.rockstars.web.WebMethodFailedListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ObserverActivity, WebMethodFailedListener {
    public static final String PREF_FILE_NAME = "pref";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHOTO_URI = "user_photo";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private AppBarLayout mAppbar;
    private View mSplashScreen;
    private Service mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAppbar = (AppBarLayout) findViewById(R.id.appbar);
        mAppbar.setVisibility(View.GONE);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        final AppCompatActivity activity = this;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                setTitle(mSectionsPagerAdapter.getPageTitle(position));
                ActivityCompat.invalidateOptionsMenu(activity);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

        mSplashScreen = findViewById(R.id.splashScreen);
        mService = new Service(this);
        RockstarsTask task = new RockstarsTask(this);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupTabIcons() {
        mTabLayout.getTabAt(0).setIcon(R.drawable.ic_view_list_white_36dp);
        mTabLayout.getTabAt(1).setIcon(R.drawable.ic_bookmark_white_36dp);
        mTabLayout.getTabAt(2).setIcon(R.drawable.ic_person_white_36dp);
    }

    @Override
    public void onTabContentChanged(int section) {
        switch (section) {
            case RockstarsFragment.SECTION:
                ((ObservableTab) mSectionsPagerAdapter.getItem(BookmarksFragment.SECTION)).update();
                break;
            case BookmarksFragment.SECTION:
                ((ObservableTab) mSectionsPagerAdapter.getItem(RockstarsFragment.SECTION)).update();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == ProfileFragment.PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL) {
            mSectionsPagerAdapter.getItem(2).onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProfileFragment.REQUEST_SELECT_PICTURE) {
            mSectionsPagerAdapter.getItem(2).onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onWebMethodFailed(int code, String message) {
        if (message == null){
            message = getString(R.string.error_internet);
        }
        ErrorDialog dialog = new ErrorDialog();
        dialog.setErrorMessage(message);
        dialog.show(getSupportFragmentManager(), "error");

        mSplashScreen.setVisibility(View.GONE);
        mAppbar.setVisibility(View.VISIBLE);
    }

    private class RockstarsTask extends AsyncTask<Void, Void, ArrayList<Rockstar>> {
        FragmentActivity activity;

        public RockstarsTask(FragmentActivity activity) {
            this.activity = activity;
        }

        @Override
        protected ArrayList<Rockstar> doInBackground(Void... params) {
            return mService.getRockstars();
        }

        @Override
        protected void onPostExecute(ArrayList<Rockstar> result) {
            mSplashScreen.setVisibility(View.GONE);
            mAppbar.setVisibility(View.VISIBLE);
            mSectionsPagerAdapter = new SectionsPagerAdapter(activity, result);
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }

    }
}
